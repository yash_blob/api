// grab the things we need
let mongoose = require("mongoose");
let Schema = mongoose.Schema;
const idGenerator = require("./../utils/id-generator");
const labelConstants = require("./../constants/label-constants");

// create a schema
let trackerSchema = new Schema({
    tracker_base_id: {
        type: String,
        default: ""
    },
    EventType: {
        type: String,
        default: ""
    },
    EventResult: {
        type: String,
        default: ""
    },
    Prefix: {
        type: String,
        default: ""
    },
    Tags: {
        type: String,
        default: ""
    },
    MetaData: {
        type: String,
        default: ""
    },
    raw_data: {
        type: String,
        default: ""
    },
    created_at: {
        type: Date,
        default: Date.now
    }
});

trackerSchema.pre("save", function (callback) {
    idGenerator.generateId(
        "tracker",
        "tracker_base_id",
        labelConstants.Tracker,
        (err, ID) => {
            this.tracker_base_id = ID;
            callback();
        }
    );
});

let Part = mongoose.model("Tracker", trackerSchema);
module.exports = Part;
