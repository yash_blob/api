"use strict";

const responseCodes = require("../helpers/response-codes");
const logger = require("../utils/logger");
const jsonResponse = require("../utils/json-response");
const errors = require("../utils/dz-errors");
const express = require("express");
const router = express.Router();
const _ = require("underscore");
const userModelHandler = require("../model_handlers/user-handler");

router.get("/all-users", async (req, res, next) => {
  try {
    let response = await userModelHandler.allUsers(req);
    jsonResponse(res, responseCodes.OK, errors.noError(), response);
  } catch (error) {
    console.log(error);
    try {
      jsonResponse(res, error.code, errors.formatErrorForWire(error), null);
    } catch (error) {
      jsonResponse(
        res,
        responseCodes.InternalServer,
        errors.internalServer(true),
        {}
      );
    }
  }
});

router.post("/click-button/:type", async (req, res, next) => {
  try {
    let response = await userModelHandler.clickingButton(req, req.params.type);
    jsonResponse(res, responseCodes.OK, errors.noError(), response);
  } catch (error) {
    console.log(error);
    try {
      jsonResponse(res, error.code, errors.formatErrorForWire(error), null);
    } catch (error) {
      jsonResponse(
        res,
        responseCodes.InternalServer,
        errors.internalServer(true),
        {}
      );
    }
  }
});


  router.get("/get-tracking-data", async (req, res, next) => {
  try {
    let response = await userModelHandler.getTrackingData();
    jsonResponse(res, responseCodes.OK, errors.noError(), response);
  } catch (error) {
    console.log(error);
    try {
      jsonResponse(res, error.code, errors.formatErrorForWire(error), null);
    } catch (error) {
      jsonResponse(
          res,
          responseCodes.InternalServer,
          errors.internalServer(true),
          {}
      );
    }
  }
});

router.get("/get-tracking-data/:id", async (req, res, next) => {
  try {
    let response = await userModelHandler.getTrackingDataById(req.params.id);
    jsonResponse(res, responseCodes.OK, errors.noError(), response);
  } catch (error) {
    console.log(error);
    try {
      jsonResponse(res, error.code, errors.formatErrorForWire(error), null);
    } catch (error) {
      jsonResponse(
          res,
          responseCodes.InternalServer,
          errors.internalServer(true),
          {}
      );
    }
  }
});

router.post("/log-event", async (req, res, next) => {
  try {
    let response = await userModelHandler.logEvent(req);
    jsonResponse(res, responseCodes.OK, errors.noError(), response);
  } catch (error) {
    console.log(error);
    try {
      jsonResponse(res, error.code, errors.formatErrorForWire(error), null);
    } catch (error) {
      jsonResponse(
          res,
          responseCodes.InternalServer,
          errors.internalServer(true),
          {}
      );
    }
  }
});
module.exports = router;
