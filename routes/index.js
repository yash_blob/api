'use strict';

const express = require('express');
const router = express.Router();
const responseCodes = require('./../helpers/response-codes');
const logger = require('./../utils/logger');
const jsonResponse = require('./../utils/json-response');
const errors = require('./../utils/dz-errors');

/* GET home page. */
router.get('/', function(req, res) {
    res.render('/index.html', {
        foo: 'bar'
    });
});

/* View api documentation */
router.get('/api-doc', (req, res) => {
    res.redirect('/mobi-api-doc/apidoc/index.html');
});

module.exports = router;