"use strict";
/*
 * Related collection name with a database & schema
 */
module.exports = {
  dbSchema: {
    administrators: "Administrator",
    tracker: "Tracker",
  },
};
