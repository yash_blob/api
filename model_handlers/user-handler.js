"use strict";

const logger = require("../utils/logger");
const jsonResponse = require("../utils/json-response");
const errors = require("../utils/dz-errors");
const TrackerHelper = require("../helpers/TrackerHelper");
const queryCreator = require("../utils/query-creator");
const { dbSchema } = require("../constants/db-constants");


const allUsers = (ReqParams) => {
  return new Promise(async (resolve, reject) => {
    try {
      let userData = [
        {
          id: 1,
          first_name: "Fayth",
          last_name: "Paulin",
          email: "fpaulin0@yellowbook.com",
          gender: "Female",
        },
        {
          id: 2,
          first_name: "Nadiya",
          last_name: "Gorring",
          email: "ngorring1@geocities.com",
          gender: "Female",
        },
        {
          id: 3,
          first_name: "Reynolds",
          last_name: "Sawfoot",
          email: "rsawfoot2@acquirethisname.com",
          gender: "Male",
        },
        {
          id: 4,
          first_name: "Marijo",
          last_name: "Ioselevich",
          email: "mioselevich3@squarespace.com",
          gender: "Female",
        },
        {
          id: 5,
          first_name: "Wylie",
          last_name: "Noblett",
          email: "wnoblett4@auda.org.au",
          gender: "Male",
        },
        {
          id: 6,
          first_name: "Janis",
          last_name: "Hover",
          email: "jhover5@vkontakte.ru",
          gender: "Female",
        },
        {
          id: 7,
          first_name: "Abbie",
          last_name: "Hiner",
          email: "ahiner6@yandex.ru",
          gender: "Male",
        },
        {
          id: 8,
          first_name: "Cornie",
          last_name: "Clutterbuck",
          email: "cclutterbuck7@google.fr",
          gender: "Male",
        },
        {
          id: 9,
          first_name: "Tabitha",
          last_name: "Hartas",
          email: "thartas8@newyorker.com",
          gender: "Female",
        },
        {
          id: 10,
          first_name: "Rachele",
          last_name: "Goater",
          email: "rgoater9@bigcartel.com",
          gender: "Female",
        },
        {
          id: 11,
          first_name: "Cosimo",
          last_name: "Ayers",
          email: "cayersa@wikispaces.com",
          gender: "Male",
        },
        {
          id: 12,
          first_name: "Matthus",
          last_name: "Bapty",
          email: "mbaptyb@epa.gov",
          gender: "Male",
        },
        {
          id: 13,
          first_name: "Cathe",
          last_name: "Mallon",
          email: "cmallonc@delicious.com",
          gender: "Female",
        },
        {
          id: 14,
          first_name: "Andras",
          last_name: "Noades",
          email: "anoadesd@geocities.com",
          gender: "Male",
        },
        {
          id: 15,
          first_name: "Pincus",
          last_name: "Kissick",
          email: "pkissicke@paypal.com",
          gender: "Male",
        },
        {
          id: 16,
          first_name: "Everard",
          last_name: "Sheals",
          email: "eshealsf@live.com",
          gender: "Male",
        },
        {
          id: 17,
          first_name: "Hymie",
          last_name: "Barles",
          email: "hbarlesg@diigo.com",
          gender: "Male",
        },
        {
          id: 18,
          first_name: "Pooh",
          last_name: "Glasscoe",
          email: "pglasscoeh@cnet.com",
          gender: "Male",
        },
        {
          id: 19,
          first_name: "Fern",
          last_name: "Trask",
          email: "ftraski@bing.com",
          gender: "Female",
        },
        {
          id: 20,
          first_name: "Rolph",
          last_name: "Evett",
          email: "revettj@list-manage.com",
          gender: "Male",
        },
        {
          id: 21,
          first_name: "Cam",
          last_name: "Martins",
          email: "cmartinsk@icio.us",
          gender: "Male",
        },
        {
          id: 22,
          first_name: "Lind",
          last_name: "Rosendale",
          email: "lrosendalel@ebay.com",
          gender: "Female",
        },
        {
          id: 23,
          first_name: "Randall",
          last_name: "Breawood",
          email: "rbreawoodm@dell.com",
          gender: "Male",
        },
        {
          id: 24,
          first_name: "Jimmy",
          last_name: "Cloy",
          email: "jcloyn@columbia.edu",
          gender: "Male",
        },
        {
          id: 25,
          first_name: "Ezmeralda",
          last_name: "Wiggington",
          email: "ewiggingtono@usda.gov",
          gender: "Female",
        },
        {
          id: 26,
          first_name: "Phip",
          last_name: "Maycock",
          email: "pmaycockp@abc.net.au",
          gender: "Male",
        },
        {
          id: 27,
          first_name: "Ciro",
          last_name: "Duns",
          email: "cdunsq@cbc.ca",
          gender: "Male",
        },
        {
          id: 28,
          first_name: "Barr",
          last_name: "Jansson",
          email: "bjanssonr@telegraph.co.uk",
          gender: "Male",
        },
        {
          id: 29,
          first_name: "Daffi",
          last_name: "Duffil",
          email: "dduffils@twitter.com",
          gender: "Female",
        },
        {
          id: 30,
          first_name: "Doy",
          last_name: "Peckham",
          email: "dpeckhamt@eepurl.com",
          gender: "Male",
        },
        {
          id: 31,
          first_name: "Robinetta",
          last_name: "Murdy",
          email: "rmurdyu@craigslist.org",
          gender: "Female",
        },
        {
          id: 32,
          first_name: "Tracey",
          last_name: "Cornner",
          email: "tcornnerv@house.gov",
          gender: "Male",
        },
        {
          id: 33,
          first_name: "Orelle",
          last_name: "Lambal",
          email: "olambalw@google.pl",
          gender: "Female",
        },
        {
          id: 34,
          first_name: "Tod",
          last_name: "Wardhaugh",
          email: "twardhaughx@vinaora.com",
          gender: "Male",
        },
        {
          id: 35,
          first_name: "Esdras",
          last_name: "Del Checolo",
          email: "edelchecoloy@example.com",
          gender: "Male",
        },
        {
          id: 36,
          first_name: "Hadleigh",
          last_name: "Gerritsma",
          email: "hgerritsmaz@instagram.com",
          gender: "Male",
        },
        {
          id: 37,
          first_name: "Rick",
          last_name: "Ilyushkin",
          email: "rilyushkin10@trellian.com",
          gender: "Male",
        },
        {
          id: 38,
          first_name: "Stevana",
          last_name: "Crumley",
          email: "scrumley11@i2i.jp",
          gender: "Female",
        },
        {
          id: 39,
          first_name: "Minna",
          last_name: "Wulfinger",
          email: "mwulfinger12@state.tx.us",
          gender: "Female",
        },
        {
          id: 40,
          first_name: "Caleb",
          last_name: "Ravelus",
          email: "cravelus13@indiegogo.com",
          gender: "Male",
        },
        {
          id: 41,
          first_name: "Ronnica",
          last_name: "Drugan",
          email: "rdrugan14@t-online.de",
          gender: "Female",
        },
        {
          id: 42,
          first_name: "Gradeigh",
          last_name: "Wainman",
          email: "gwainman15@sciencedaily.com",
          gender: "Male",
        },
        {
          id: 43,
          first_name: "Karyn",
          last_name: "Skippen",
          email: "kskippen16@fema.gov",
          gender: "Female",
        },
        {
          id: 44,
          first_name: "Jaime",
          last_name: "Manjin",
          email: "jmanjin17@weebly.com",
          gender: "Male",
        },
        {
          id: 45,
          first_name: "Oswell",
          last_name: "Chable",
          email: "ochable18@baidu.com",
          gender: "Male",
        },
        {
          id: 46,
          first_name: "Michele",
          last_name: "Dyka",
          email: "mdyka19@amazonaws.com",
          gender: "Female",
        },
        {
          id: 47,
          first_name: "Jameson",
          last_name: "Vigietti",
          email: "jvigietti1a@ustream.tv",
          gender: "Male",
        },
        {
          id: 48,
          first_name: "Candra",
          last_name: "Louys",
          email: "clouys1b@epa.gov",
          gender: "Female",
        },
        {
          id: 49,
          first_name: "Stanislaus",
          last_name: "Livingston",
          email: "slivingston1c@w3.org",
          gender: "Male",
        },
        {
          id: 50,
          first_name: "Teodoro",
          last_name: "Howick",
          email: "thowick1d@topsy.com",
          gender: "Male",
        },
        {
          id: 51,
          first_name: "Bessy",
          last_name: "Brandli",
          email: "bbrandli1e@phpbb.com",
          gender: "Female",
        },
        {
          id: 52,
          first_name: "Dotty",
          last_name: "Bance",
          email: "dbance1f@nba.com",
          gender: "Female",
        },
        {
          id: 53,
          first_name: "Vivyanne",
          last_name: "Guiducci",
          email: "vguiducci1g@sitemeter.com",
          gender: "Female",
        },
        {
          id: 54,
          first_name: "Carlynne",
          last_name: "Stobbie",
          email: "cstobbie1h@goo.gl",
          gender: "Female",
        },
        {
          id: 55,
          first_name: "Una",
          last_name: "Midson",
          email: "umidson1i@weibo.com",
          gender: "Female",
        },
        {
          id: 56,
          first_name: "Matthew",
          last_name: "Woodstock",
          email: "mwoodstock1j@usatoday.com",
          gender: "Male",
        },
        {
          id: 57,
          first_name: "Elie",
          last_name: "Rother",
          email: "erother1k@xinhuanet.com",
          gender: "Female",
        },
        {
          id: 58,
          first_name: "Ivor",
          last_name: "Olver",
          email: "iolver1l@vimeo.com",
          gender: "Male",
        },
        {
          id: 59,
          first_name: "Tybi",
          last_name: "Guymer",
          email: "tguymer1m@ovh.net",
          gender: "Female",
        },
        {
          id: 60,
          first_name: "Mariejeanne",
          last_name: "Kersting",
          email: "mkersting1n@weather.com",
          gender: "Female",
        },
        {
          id: 61,
          first_name: "Kimberlyn",
          last_name: "Burcher",
          email: "kburcher1o@skyrock.com",
          gender: "Female",
        },
        {
          id: 62,
          first_name: "Justin",
          last_name: "Trundell",
          email: "jtrundell1p@angelfire.com",
          gender: "Male",
        },
        {
          id: 63,
          first_name: "Sher",
          last_name: "Wrangle",
          email: "swrangle1q@blogger.com",
          gender: "Female",
        },
        {
          id: 64,
          first_name: "Stella",
          last_name: "Antczak",
          email: "santczak1r@smh.com.au",
          gender: "Female",
        },
        {
          id: 65,
          first_name: "John",
          last_name: "Wandrey",
          email: "jwandrey1s@yellowbook.com",
          gender: "Male",
        },
        {
          id: 66,
          first_name: "Doro",
          last_name: "Lochet",
          email: "dlochet1t@friendfeed.com",
          gender: "Female",
        },
        {
          id: 67,
          first_name: "Ingeborg",
          last_name: "Vasiliu",
          email: "ivasiliu1u@youku.com",
          gender: "Female",
        },
        {
          id: 68,
          first_name: "Cyndie",
          last_name: "Willets",
          email: "cwillets1v@360.cn",
          gender: "Female",
        },
        {
          id: 69,
          first_name: "Breena",
          last_name: "Whybrow",
          email: "bwhybrow1w@cmu.edu",
          gender: "Female",
        },
        {
          id: 70,
          first_name: "Franny",
          last_name: "Gillbe",
          email: "fgillbe1x@adobe.com",
          gender: "Female",
        },
        {
          id: 71,
          first_name: "Quintus",
          last_name: "Ralls",
          email: "qralls1y@amazon.com",
          gender: "Male",
        },
        {
          id: 72,
          first_name: "Megan",
          last_name: "Hars",
          email: "mhars1z@woothemes.com",
          gender: "Female",
        },
        {
          id: 73,
          first_name: "Lucian",
          last_name: "Batsford",
          email: "lbatsford20@hostgator.com",
          gender: "Male",
        },
        {
          id: 74,
          first_name: "Guenna",
          last_name: "O'Sharkey",
          email: "gosharkey21@adobe.com",
          gender: "Female",
        },
        {
          id: 75,
          first_name: "Harold",
          last_name: "Hill",
          email: "hhill22@people.com.cn",
          gender: "Male",
        },
        {
          id: 76,
          first_name: "Faber",
          last_name: "Glencrosche",
          email: "fglencrosche23@gizmodo.com",
          gender: "Male",
        },
        {
          id: 77,
          first_name: "Chloe",
          last_name: "Georger",
          email: "cgeorger24@un.org",
          gender: "Female",
        },
        {
          id: 78,
          first_name: "Ivette",
          last_name: "Rulten",
          email: "irulten25@oaic.gov.au",
          gender: "Female",
        },
        {
          id: 79,
          first_name: "Lorette",
          last_name: "Buggs",
          email: "lbuggs26@topsy.com",
          gender: "Female",
        },
        {
          id: 80,
          first_name: "Kylila",
          last_name: "Derl",
          email: "kderl27@webnode.com",
          gender: "Female",
        },
        {
          id: 81,
          first_name: "Delbert",
          last_name: "Caltun",
          email: "dcaltun28@com.com",
          gender: "Male",
        },
        {
          id: 82,
          first_name: "Fernando",
          last_name: "Stockford",
          email: "fstockford29@wordpress.org",
          gender: "Male",
        },
        {
          id: 83,
          first_name: "Sophey",
          last_name: "Pitcaithly",
          email: "spitcaithly2a@dyndns.org",
          gender: "Female",
        },
        {
          id: 84,
          first_name: "Lorita",
          last_name: "Peare",
          email: "lpeare2b@theatlantic.com",
          gender: "Female",
        },
        {
          id: 85,
          first_name: "Candra",
          last_name: "Whiteside",
          email: "cwhiteside2c@deviantart.com",
          gender: "Female",
        },
        {
          id: 86,
          first_name: "Mackenzie",
          last_name: "Fearn",
          email: "mfearn2d@google.com.hk",
          gender: "Male",
        },
        {
          id: 87,
          first_name: "Aldous",
          last_name: "Lubman",
          email: "alubman2e@fastcompany.com",
          gender: "Male",
        },
        {
          id: 88,
          first_name: "Irvin",
          last_name: "Reckhouse",
          email: "ireckhouse2f@networkadvertising.org",
          gender: "Male",
        },
        {
          id: 89,
          first_name: "Stefano",
          last_name: "Catterson",
          email: "scatterson2g@comsenz.com",
          gender: "Male",
        },
        {
          id: 90,
          first_name: "Lissa",
          last_name: "Robins",
          email: "lrobins2h@uol.com.br",
          gender: "Female",
        },
        {
          id: 91,
          first_name: "Garrek",
          last_name: "Lake",
          email: "glake2i@wix.com",
          gender: "Male",
        },
        {
          id: 92,
          first_name: "Bartholomeus",
          last_name: "Duchasteau",
          email: "bduchasteau2j@elegantthemes.com",
          gender: "Male",
        },
        {
          id: 93,
          first_name: "Deane",
          last_name: "Edwardes",
          email: "dedwardes2k@wikia.com",
          gender: "Female",
        },
        {
          id: 94,
          first_name: "Kaylil",
          last_name: "Packer",
          email: "kpacker2l@elegantthemes.com",
          gender: "Female",
        },
        {
          id: 95,
          first_name: "Angie",
          last_name: "Clerk",
          email: "aclerk2m@bluehost.com",
          gender: "Female",
        },
        {
          id: 96,
          first_name: "Richardo",
          last_name: "Clissett",
          email: "rclissett2n@eepurl.com",
          gender: "Male",
        },
        {
          id: 97,
          first_name: "Gerry",
          last_name: "Somner",
          email: "gsomner2o@opensource.org",
          gender: "Male",
        },
        {
          id: 98,
          first_name: "Rowen",
          last_name: "Pover",
          email: "rpover2p@51.la",
          gender: "Male",
        },
        {
          id: 99,
          first_name: "Oralla",
          last_name: "Balk",
          email: "obalk2q@ifeng.com",
          gender: "Female",
        },
        {
          id: 100,
          first_name: "Merl",
          last_name: "Shapera",
          email: "mshapera2r@typepad.com",
          gender: "Female",
        },
      ];
      resolve(userData);
      let RawData = {
        userList: "listing users",
        button_name: "list user button clicked",
        button_type: "popup button",
      };
      TrackerHelper.AddEvent(
        "listing_event",
        "success",
        "TestProject_listing",
        "listing,user listing,get listing",
        ReqParams,
        RawData
      );
    } catch (error) {
      reject(error);
    }
  });
};

const clickingButton = (ReqParams, ButtonType) => {
  return new Promise(async (resolve, reject) => {
    try {
      resolve(ButtonType);
      let RawData = {
        button_click: "clicking register button",
        button_name: "Register Now",
        button_type: ButtonType,
        button_color: "red",
        form_data: "you can also pass form data",
      };
      TrackerHelper.AddEvent(
        "buttonClick_event",
        "success",
        "NewProject_ButtonClick",
        "button click, Register button click,red button,register form",
        ReqParams,
        RawData
      );
    } catch (error) {
      reject(error);
    }
  });
};
const getTrackingData = () => {
  return new Promise(async (resolve, reject) => {
    try {
      let GettingData = await queryCreator.selectWithAndPromise(dbSchema.tracker,{},{});
      resolve(GettingData)
    } catch (error) {
      reject(error);
    }
  });
};

const getTrackingDataById = (Id) => {
  return new Promise(async (resolve, reject) => {
    try {
      let GettingData = await queryCreator.selectWithAndOnePromise(dbSchema.tracker,{tracker_base_id:Id},{});
      resolve(GettingData)
    } catch (error) {
      reject(error);
    }
  });
};
const logEvent = (ReqParams) => {
  return new Promise(async (resolve, reject) => {
    try {

      resolve(ReqParams.body)
    } catch (error) {
      reject(error);
    }
  });
};
module.exports = {
  allUsers,
  clickingButton,
  getTrackingData,
  getTrackingDataById,
  logEvent
};
