const jwt = require('jsonwebtoken');
require('dotenv').config();
const JWTsecret = process.env.SECRET;


const Auth = (ReqParams) => {
    return new Promise(async (resolve, reject) => {
        try {
            jwt.verify(ReqParams, JWTsecret, function(err, decoded) {
                if (err){
                    reject({ auth: false, message: 'Failed to authenticate token.' })
                    return;
                }
                //TODO YASH here we will check if user is in database and refresh token


                resolve(decoded);
            });
        } catch (error) {
            console.log(error);
            reject(error);
        }
    });
};


module.exports = {
    Auth
};
