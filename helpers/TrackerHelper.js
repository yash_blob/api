const GeoIp = require("geoip-lite");
const queryCreator = require("../utils/query-creator");
const { dbSchema } = require("../constants/db-constants");
const Tracker = require("../models/tracker");
//EventType = signup_event / register_event / product_get_event
//EventResult = success / error
//Prefix = projectId_userID_122342 / projectId_product
//Tags  = simple tags to remember this action
const AddEvent = (EventType, EventResult, Prefix, Tags, MetaData, raw_data) => {
  return new Promise(async (resolve, reject) => {
    try {
      let geo = GeoIp.lookup(MetaData.ip);
      let MetaDataInsert = {
        Headers: JSON.stringify(MetaData.headers),
        IP: JSON.stringify(MetaData.ip),
        Browser: MetaData.headers["user-agent"],
        Language: MetaData.headers["accept-language"],
        Country: geo ? geo.country : "Unknown",
        Region: geo ? geo.region : "Unknown",
      };
      let DataToInsert = {
        EventType: EventType ? EventType : "TestProject_Unknown",
        EventResult: EventResult ? EventResult : "TestProject_Unknown",
        Prefix: Prefix ? Prefix : "TestProject_Unknown",
        Tags: Tags ? Tags : "",
        MetaData: JSON.stringify(MetaDataInsert),
        raw_data: JSON.stringify(raw_data),
      };
      queryCreator.insertSinglePromise(dbSchema.tracker, DataToInsert);
    } catch (error) {
      reject(error);
    }
  });
};
module.exports = {
  AddEvent,
};
