'use strict';
//configurations
const path = require('path');
const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const logger = require('./utils/logger');
const jsonResponse = require('./utils/json-response');
const errors = require('./utils/dz-errors');
const JWT = require('./helpers/jwt_auth')
const cors = require('cors');
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');


//express configurations
const app = express();

const swaggerOptions = {
	swaggerDefinition:{
		info:{
			title:"Tracker API",
			description:"Tracker API documentation",
			server: "http://localhost:4800"
		}
	},
	apis:["./routes/users.js"]
}
const swaggerDocument = require('./swagger.json');
const swaggerDocs = swaggerJSDoc(swaggerOptions);
app.use("/api-docs",swaggerUi.serve,swaggerUi.setup(swaggerDocument))

//routes for Backend
const routes = require('./routes/index');


//routes for API

const user = require("./routes/users");


//other configurations 
const favicon = require('serve-favicon');
const multiparty = require('connect-multiparty');
const upload = require('express-fileupload');
const multipartyMiddleWare = multiparty();
 




// app.use(favicon(path.join(__dirname, './public/img', 'favicon.png')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json({limit: '5000mb'}));
app.use(bodyParser.urlencoded({extended: true, limit: '5000mb'}));
app.use(cookieParser());
app.use(require('express-session')({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
}));
app.use(cors()); 
app.use(multipartyMiddleWare); 

//main API logger
let logUrl = () => {  
	return (req, res, next) => {
	  let url = req.url;
	  console.log("This is route =>", url); 
	  next(); 
	};
 };


// import routes for backend
app.use('/',routes);
app.use("/api/user/", logUrl(), user);



app.use(upload());


// catch 404 and forward to error handler
app.use((req, res) => {
	logger('Error: No route found or Wrong method name');
	jsonResponse(res, 404,errors.resourceNotFound(true), null);
});

// will print stacktrace

if (app.get('env') === 'development') {
	app.use((err, req, res) => {
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
			error: err
		});
	});
}

// production error handler
// no stack traces leaked to user
app.use((err, req, res) => {
	res.status(err.status || 500);
	res.render('error', {
	message: err.message,
		error: {}
	});
});

module.exports = app;



