'use strict';
require('dotenv').config()

module.exports = {
    timezone: process.env.TIME_ZONE,
    appName: "Tracker App",
    port:4800,
  
};


