const cloudinary = require('cloudinary').v2;
const errors = require('./dz-errors')
require('dotenv').config()


cloudinary.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key:  process.env.API_KEY,
    api_secret: process.env.API_SECRET
});
const fs = require('fs');


const uploadFileObj = function(fileObject) {
    return new Promise(async (resolve, reject) => {
        try {
            let image = await  cloudinary.uploader.upload(fileObject.path, { tags: 'basic_sample' });
            resolve(image.url)
        } catch (error) {
            console.log(error)
            reject(error);
        }
    });
};
module.exports = {
    uploadFileObj
};