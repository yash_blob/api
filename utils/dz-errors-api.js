'use strict';

const _ = require('underscore');
const responseCodes = require('./../helpers/response-codes');
const labels = require('./../utils/labels.json');
const config = require('../config');

function DZError(message, code, name = 'DZError') {
	this.name = name;
	this.message = message || 'Default Message';
	this.code = code;
	this.stack = (new Error()).stack;
}

DZError.prototype = Object.create(Error.prototype);
DZError.prototype.constructor = DZError;

module.exports = {
	missingParameters: function(formatForWire, language){
		const error = new DZError(
			labels.LBL_MISSING_PARAMETERS['MESSAGE'][language || config.default_language],
			responseCodes.BadRequest,
			labels.LBL_MISSING_PARAMETERS['NAME'][language || config.default_language]
		);
		return formatForWire ? this.formatErrorForWire(error) : error;
	},
	internalServer: function(formatForWire, language){
		const error = new DZError(
			labels.LBL_INTERNAL_SERVER['MESSAGE'][language || config.default_language],
			responseCodes.InternalServer,
			labels.LBL_INTERNAL_SERVER['NAME'][language || config.default_language]
		);
		return formatForWire ? this.formatErrorForWire(error) : error;
	},
	userNotFound: function(formatForWire, language){
		const error = new DZError(
			labels.LBL_USER_NOT_FOUND['MESSAGE'][language || config.default_language],
			responseCodes.ResourceNotFound, 
			labels.LBL_USER_NOT_FOUND['NAME'][language || config.default_language],
		);
		return formatForWire ? this.formatErrorForWire(error) : error;
	},
	faqCategoryNotFound: function(formatForWire, language){
		const error = new DZError(
			labels.LBL_FAQ_CATEGORY_NOT_FOUND['MESSAGE'][language || config.default_language],
			responseCodes.ResourceNotFound, 
			labels.LBL_FAQ_CATEGORY_NOT_FOUND['NAME'][language || config.default_language],
		);
		return formatForWire ? this.formatErrorForWire(error) : error;
	},
	templateNotFound: function(formatForWire, language){
		const error = new DZError(
			labels.LBL_TEMPLATE_NOT_FOUND['MESSAGE'][language || config.default_language],
			responseCodes.ResourceNotFound, 
			labels.LBL_TEMPLATE_NOT_FOUND['NAME'][language || config.default_language],
		);
		return formatForWire ? this.formatErrorForWire(error) : error;
	},
	invalidOTP: function(formatForWire, language) {
		const error = new DZError(
			labels.LBL_INVALID_OTP['MESSAGE'][language || config.default_language],
			responseCodes.InvalidOTP, 
			labels.LBL_INVALID_OTP['MESSAGE'][language || config.default_language]
		);
		return formatForWire ? this.formatErrorForWire(error) : error;
	},
	resourceNotFound: function(formatForWire, language){
		const error = new DZError(
			labels.LBL_CUSTOMER_NOT_FOUND['MESSAGE'][language || config.default_language],
			responseCodes.ResourceNotFound, 
			labels.LBL_CUSTOMER_NOT_FOUND['NAME'][language || config.default_language],
		);
		return formatForWire ? this.formatErrorForWire(error) : error;
	},
	emailAlreadyExist: function(formatForWire, language){
		const error = new DZError(
			labels.LBL_EMAIL_ALREADY_EXIST['MESSAGE'][language || config.default_language],
			responseCodes.Conflict, 
			labels.LBL_EMAIL_ALREADY_EXIST['NAME'][language || config.default_language],
		);
		return formatForWire ? this.formatErrorForWire(error) : error;
	},
	alreadyAssigned: function(formatForWire, language){
		const error = new DZError(
			labels.LBL_ALREADY_ASSIGNED['MESSAGE'][language || config.default_language],
			responseCodes.Conflict, 
			labels.LBL_ALREADY_ASSIGNED['NAME'][language || config.default_language],
		);
		return formatForWire ? this.formatErrorForWire(error) : error;
	},
	alreadyBidded: function(formatForWire, language){
		const error = new DZError(
			labels.LBL_ALREADY_BIDDED['MESSAGE'][language || config.default_language],
			responseCodes.Conflict, 
			labels.LBL_ALREADY_BIDDED['NAME'][language || config.default_language],
		);
		return formatForWire ? this.formatErrorForWire(error) : error;
	},
	mobileAlreadyExist: function(formatForWire, language){
		const error = new DZError(
			labels.LBL_MOBILE_ALREADY_EXIST['MESSAGE'][language || config.default_language],
			responseCodes.Conflict, 
			labels.LBL_MOBILE_ALREADY_EXIST['NAME'][language || config.default_language],
		);
		return formatForWire ? this.formatErrorForWire(error) : error;
	},
	socialIdAlreadyExist: function(formatForWire, language){
		const error = new DZError(
			labels.LBL_SOCIAL_ID_ALREADY_EXIST['MESSAGE'][language || config.default_language],
			responseCodes.Conflict, 
			labels.LBL_SOCIAL_ID_ALREADY_EXIST['NAME'][language || config.default_language],
		);
		return formatForWire ? this.formatErrorForWire(error) : error;
	},
	socialIdNotFound: function(formatForWire, language){
		const error = new DZError(
			labels.LBL_SOCIAL_ID_NOT_FOUND['MESSAGE'][language || config.default_language],
			responseCodes.ResourceNotFound, 
			labels.LBL_SOCIAL_ID_NOT_FOUND['NAME'][language || config.default_language],
		);
		return formatForWire ? this.formatErrorForWire(error) : error;
	},
	emailNotFound: function(formatForWire, language){
		const error = new DZError(
			labels.LBL_EMAIL_NOT_FOUND['MESSAGE'][language || config.default_language],
			responseCodes.ResourceNotFound, 
			labels.LBL_EMAIL_NOT_FOUND['NAME'][language || config.default_language],
		);
		return formatForWire ? this.formatErrorForWire(error) : error;
	},
	mobileNotFound: function(formatForWire, language){
		const error = new DZError(
			labels.LBL_MOBILE_NOT_FOUND['MESSAGE'][language || config.default_language],
			responseCodes.ResourceNotFound, 
			labels.LBL_MOBILE_NOT_FOUND['NAME'][language || config.default_language],
		);
		return formatForWire ? this.formatErrorForWire(error) : error;
	},
	tripNotFound: function(formatForWire, language){
		const error = new DZError(
			labels.LBL_TRIP_NOT_FOUND['MESSAGE'][language || config.default_language],
			responseCodes.ResourceNotFound, 
			labels.LBL_TRIP_NOT_FOUND['NAME'][language || config.default_language],
		);
		return formatForWire ? this.formatErrorForWire(error) : error;
	},
	invalidPassword: function(formatForWire, language){
		const error = new DZError(
			labels.LBL_INVALID_PASSWORD['MESSAGE'][language || config.default_language],
			responseCodes.Invalid, 
			labels.LBL_INVALID_PASSWORD['NAME'][language || config.default_language],
		);
		return formatForWire ? this.formatErrorForWire(error) : error;
	},
	invalidOldPassword: function(formatForWire, language){
		const error = new DZError(
			labels.LBL_INVALID_OLD_PASSWORD['MESSAGE'][language || config.default_language],
			responseCodes.Invalid, 
			labels.LBL_INVALID_OLD_PASSWORD['NAME'][language || config.default_language],
		);
		return formatForWire ? this.formatErrorForWire(error) : error;
	},
	duplicateUser: function(formatForWire, language){
		const error = new DZError(
			labels.LBL_CUSTOMER_NOT_FOUND['MESSAGE'][language || config.default_language],
			responseCodes.Conflict, 
			labels.LBL_CUSTOMER_NOT_FOUND['NAME'][language || config.default_language],
		);
		return formatForWire ? this.formatErrorForWire(error) : error;
	},
	notActivate: function(formatForWire, language){
		const error = new DZError(
			labels.LBL_ACCOUNT_INACTIVE['MESSAGE'][language || config.default_language],
			responseCodes.NotActive,
			labels.LBL_ACCOUNT_INACTIVE['NAME'][language || config.default_language]
		);
		return formatForWire ? this.formatErrorForWire(error) : error;
	},
	noError: function(){
		return null;
	},
	errorWithMessage: function(error){
		return new DZError((_.has(error, 'message') ? error.message : ''));
	},
	formatErrorForWire: function(DZError){
		return _.omit(DZError, 'stack');
	},
	customError: function(message, code, name, formatForWire){
		const error = new DZError(message, code, name);
		return formatForWire ? this.formatErrorForWire(error) : error;
	}
};
