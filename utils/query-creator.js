"use strict";

const _ = require("underscore");
const mongoose = require("mongoose");
const logger = require("./../utils/logger");
const dbConnection = require("./../utils/db-connection");

module.exports = {
   
  selectWithAndSortPromise: function(
    collectionName,
    comparisonColumnsAndValues,
    columnsToSelect,
    sizePerPage,
    page
  ) {
    return new Promise((resolve, reject) => {
      const dbCollection = mongoose.model(collectionName);
      page = parseInt(page);
      sizePerPage = parseInt(sizePerPage);
      dbCollection
        .find(comparisonColumnsAndValues, columnsToSelect, function(
          error,
          data
        ) {
          if (error) {
            logger("Error: making async main query");
            reject(error);
            return;
          }
          resolve(data);
        })
        .sort({
          _id: -1
        })
        .limit(sizePerPage)
        .skip(sizePerPage * page);
    });
  }, 
  joinWithAndPromise: function(collectionName, params) {
    return new Promise((resolve, reject) => {
      const dbCollection = mongoose.model(collectionName);
      dbCollection.aggregate(params).exec(function(error, data) {
        if (error) {
          logger(error);
          reject(error);
          return;
        }
        resolve(data);
      });
    });
  },
 
  countRecordPromise: function(collectionName, comparisonColumnsAndValues) {
    return new Promise((resolve, reject) => {
      const dbCollection = mongoose.model(collectionName);

      dbCollection.count(comparisonColumnsAndValues, function(
        error,
        numOfDocs
      ) {
        if (error) {
          logger("Error: making async main query");
          reject(error);
          return;
        }
        resolve(numOfDocs);
      });
    });
  }, 
  insertMultiplePromise: function(collectionName, columnsAndValues) {
    return new Promise((resolve, reject) => {
      const dbCollection = mongoose.model(collectionName);
      dbCollection.insertMany(columnsAndValues, function(error, data) {
        if (error) {
          logger(error);
          reject(error);
          return;
        }
        resolve(data);
      });
    });
  },  

  updateMultiplePromise: function(
    collectionName,
    columnsToUpdate,
    targetColumnsAndValues
  ) {
    return new Promise((resolve, reject) => {
      try {
        const dbCollection = mongoose.model(collectionName);
        var options = {
          multi: true
        };
        dbCollection.updateMany(
          targetColumnsAndValues,
          {
            $set: columnsToUpdate
          },
          options,
          function(error, data) {
            if (error) {
              logger("Error: can not update document");
              reject(error);
              return;
            }
            resolve(data);
            return;
          }
        );
      } catch (error) {
        reject(error);
        return;
      }
    });
  },
 

  removeMultiplePromise: function(collectionName, targetColumnsAndValues) {
    return new Promise((resolve, reject) => {
      const dbCollection = mongoose.model(collectionName);
      dbCollection.remove(targetColumnsAndValues, function(error, data) {
        if (error) {
          logger(error);
          reject(error);
          return;
        }
        resolve(data);
      });
    });
  },

  
  updateSinglWithUpdateRecodePromiseMod: function(
    collectionName,
    columnsToUpdate,
    targetColumnsAndValues,
    columnToSelect,
    key
  ) {
    return new Promise((resolve, reject) => {
      try {
        const dbCollection = mongoose.model(collectionName);
        dbCollection.findOneAndUpdate(
          targetColumnsAndValues,
          columnsToUpdate,
          { new: true, projection: columnToSelect },
          function(error, data) {
            if (error) {
              console.log("errpr=>" + error);
              logger("Error: making async main query");
              reject(error);
              return;
            }
            if (!data) {
              reject(errors.resourceNotFound(true));
              return;
            }
            resolve(data);
            return;
          }
        );
      } catch (error) {}
    });
  },

  /**
   * Inserts single record
   *
   * @collectionName {string} collectionName to select from
   * @param {object} columnsAndValues Values and column
   *     to insert
   * @returns {object} - JSON object
   */
  insertSinglePromise: function(collectionName, columnsAndValues) {
    return new Promise((resolve, reject) => {
      const dbCollection = mongoose.model(collectionName);
      dbCollection.create(columnsAndValues, function(error, data) {
        if (error) {
          logger(error);
          reject(error);
          return;
        }
        resolve(data);
        return;
      });
    });
  },

  updateSinglWithUpdateRecodePromise: function(
    collectionName,
    columnsToUpdate,
    targetColumnsAndValues,
    columnToSelect
  ) {
    return new Promise((resolve, reject) => {
      const dbCollection = mongoose.model(collectionName);
      dbCollection.findOneAndUpdate(
        targetColumnsAndValues,
        columnsToUpdate,
        { new: true, projection: columnToSelect },
        function(error, data) {
          if (error) {
            console.log("errpr=>" + error);
            logger("Error: making async main query");
            reject(error);
            return;
          }
          resolve(data);
          return;
        }
      );
    });
  },
  selectWithAndOnePromise: function(
    collectionName,
    comparisonColumnsAndValues,
    columnsToSelect
  ) {
    return new Promise((resolve, reject) => {
      const dbCollection = mongoose.model(collectionName);
      dbCollection.findOne(
        comparisonColumnsAndValues,
        columnsToSelect,
        function(error, data) {
          if (error) {
            logger("Error: making async main query");
            reject(error, null);
            return;
          }
          resolve(data);
          return;
        }
      );
    });
  },
  updateSinglePromise: (
    collectionName,
    columnsToUpdate,
    targetColumnsAndValues
  ) => {
    return new Promise((resolve, reject) => {
      const dbCollection = mongoose.model(collectionName);
      var options = {
        multi: true
      };
      dbCollection.update(
        targetColumnsAndValues,
        columnsToUpdate,
        options,
        function(error, data) {
          if (error) {
            console.log(error);
            return false;
            logger("Error: can not update document");
            reject(error);
            return;
          }
          resolve(data);
          return;
        }
      );
    });
  },
  selectWithAndOnePromiseModified: function(
    collectionName,
    comparisonColumnsAndValues,
    columnsToSelect,
    key
  ) {
    return new Promise((resolve, reject) => {
      const dbCollection = mongoose.model(collectionName);
      dbCollection.findOne(
        comparisonColumnsAndValues,
        columnsToSelect,
        function(error, data) {
          if (error) {
            logger("Error: making async main query");
            reject(error, null);
            return;
          }
          if (!data) {
            reject(error);

            return;
          }
          resolve(data);
          return;
        }
      );
    });
  },
  selectWithAndPromiseModi: function(
    collectionName,
    comparisonColumnsAndValues,
    columnsToSelect,
    key
  ) {
    return new Promise((resolve, reject) => {
      const dbCollection = mongoose.model(collectionName);

      dbCollection.find(comparisonColumnsAndValues, columnsToSelect, function(
        error,
        data
      ) {
        if (error) {
          logger("Error: making async main query");
          reject(error);
          return;
        }
        if (!data) {
          reject(error, null);
          return;
        }
        resolve(data);
        return;
      });
    });
  },
  selectWithAndPromise: function(
    collectionName,
    comparisonColumnsAndValues,
    columnsToSelect
  ) {
    return new Promise((resolve, reject) => {
      const dbCollection = mongoose.model(collectionName);
      dbCollection.find(comparisonColumnsAndValues, columnsToSelect, function(
        error,
        data
      ) {
        if (error) {
          logger("Error: making async main query");
          reject(error, null);
          return;
        }
        resolve(data);
        return;
      });
    });
  },
  /**
   * remove  a multiple records from db
   *
   * @collectionName {string}collectionName to update
   * @columnsToUpdate {Object}  Columns and values to update
   * @targetColumnsAndValues {Object} targetColumnsAndValues to identify the update record
   *
   * @returns {object} - JSON object
   */

  removeMultipleformDb: function(
    collectionName,
    targetColumnsAndValues,
    callback
  ) {
    const dbCollection = mongoose.model(collectionName);
    dbCollection.deleteMany(targetColumnsAndValues, function(error, data) {
      if (error) {
        console.log(error);
        return false;
        logger("Error: can not update document");
        callback(error, null);
        return;
      }
      callback(null, data);
    });
  },
  selectWithAndFilterPromise: (
    collectionName,
    comparisonColumnsAndValues,
    columnsToSelect,
    columnToSort,
    columnsToPagination
  ) => {
    return new Promise((resolve, reject) => {
      const dbCollection = mongoose.model(collectionName);
      dbCollection
        .find(
          comparisonColumnsAndValues,
          columnsToSelect,
          columnsToPagination,
          function(error, data) {
            if (error) {
              logger("Error: making async main query");
              reject(error);
              return;
            }
            resolve(data);
            return;
          }
        )
        .sort(columnToSort);
    });
  }, 
};