const axios = require('axios');

const API = axios.create({
    baseURL: "https://secure.ontime360.com/sites/Vnp/api/"
});

module.exports = {
    API
};
