"use strict";
const bcrypt = require("bcrypt");
const saltRounds = 10; //for bcrypt salt
const errors = require("./dz-errors");

module.exports = {
  HashPassword: (Password, ConfirmPassword) => {
    return new Promise(async (resolve, reject) => {
      try {
        if (Password !== ConfirmPassword) {
          resolve(false);
        }
        let HashedPassword = await bcrypt.hash(Password, saltRounds);
        resolve(HashedPassword);
      } catch (error) {
        reject(errors.internalServer(true));
      }
    });
  },
  CompareHashPasswordAndOldPassword: (
    Password,
    ConfirmPassword,
    OldPassword,
    PasswordFromDb
  ) => {
    return new Promise(async (resolve, reject) => {
      try {
        if (Password !== ConfirmPassword) {
          resolve(false);
        }
        let match = await bcrypt.compare(OldPassword, PasswordFromDb);
        if (match) {
          let HashedPassword = await bcrypt.hash(Password, saltRounds);
          resolve(HashedPassword);
        } else {
          resolve(false);
        }
      } catch (error) {
        console.log(error);
        reject(errors.internalServer(true));
      }
    });
  },
  CompareHashPassword: (Password, PasswordFromDb) => {
    return new Promise(async (resolve, reject) => {
      try {
        let match = await bcrypt.compare(Password, PasswordFromDb);
        if (match) {
          resolve(match);
        } else {
          resolve(match);
        }
      } catch (error) {
        console.log(error);
        reject(errors.internalServer(true));
      }
    });
  }
};
